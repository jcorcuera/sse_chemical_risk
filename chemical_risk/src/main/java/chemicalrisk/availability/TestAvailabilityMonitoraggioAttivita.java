package chemicalrisk.availability;

import chemicalrisk.soap.SOAPClient;

public class TestAvailabilityMonitoraggioAttivita {

    // URL of the SoaML service
    private static final String WS_MONITORAGGIO_ATTIVITA = "http://localhost:8080/axis2/services/ModuloMonitoraggioAttivita";

    public static void main(String[] args) throws Exception {
        int runs = 10;
        int numberOfIterations = 1000;
        int errors[][] = new int[runs][3];
        double availabilityOperation[][] = new double[runs][3];
        double availabilityWS[] = new double[runs];
        double avgAvailability = 0;
        double avgErrors = 0;
        
        // Perform 10 runs
        for (int i = 0; i < runs; i++) {
            // Test 1000 times each operation of the module and register the number of related errors
            errors[i][0] = availabilityTestBloccaAttivita(numberOfIterations);
            errors[i][1] = availabilityTestPrelevaListaAttivitaOperatore(numberOfIterations);
            errors[i][2] = availabilityTestPrelevaAttivitaOperatore(numberOfIterations);

            // Calculate availability of each operation from #errors
            availabilityOperation[i][0] = (1 - (errors[i][0] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][1] = (1 - (errors[i][1] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][2] = (1 - (errors[i][2] * 1.0) / numberOfIterations) * 100;
            
            // Calculate the overall availability of the entire module
            availabilityWS[i] = (1 - (errors[i][0] + errors[i][1] + errors[i][2]) * 1.0 / (numberOfIterations * 3)) * 100;
            avgAvailability += availabilityWS[i];
            double errorCount = errors[i][0] + errors[i][1] + errors[i][2];
            System.out.println("--- Run #" + (i + 1) + " ---");
            System.out.println("Availability di bloccaAttivita:\t\t" + availabilityOperation[i][0]);
            System.out.println("Availability di approvaRichiesta:\t" + availabilityOperation[i][1]);
            System.out.println("Availability di rifiutaRichiesta:\t" + availabilityOperation[i][2]);
            System.out.println("Numeri d'errori:\t\t" + errorCount);
            System.out.println("Availability totale:\t\t\t" + availabilityWS[i]);
            avgErrors += errorCount;
        }

        avgAvailability = avgAvailability * 1.0 / runs;
        avgErrors = avgErrors * 1.0 / runs;

        System.out.println("Mean:\t" + avgAvailability);
        System.out.println("Avg Errors:\t" + avgErrors);
    }

    public static int availabilityTestBloccaAttivita(int numberOfIterations) {
        StringBuilder bloccaAttivitaRequest = new StringBuilder();
        bloccaAttivitaRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mon=\"http://monitoraggioattivita.axis.chemicalrisk\">");
        bloccaAttivitaRequest.append("<soapenv:Header/>");
        bloccaAttivitaRequest.append("<soapenv:Body>");
        bloccaAttivitaRequest.append("<mon:bloccaAttivita>");
        bloccaAttivitaRequest.append("<mon:attivitaOperatore>attivitaOperatore</mon:attivitaOperatore>");
        bloccaAttivitaRequest.append("</mon:bloccaAttivita>");
        bloccaAttivitaRequest.append("</soapenv:Body>");
        bloccaAttivitaRequest.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_MONITORAGGIO_ATTIVITA, bloccaAttivitaRequest.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    public static int availabilityTestPrelevaListaAttivitaOperatore(int numberOfIterations) {
        StringBuilder prelevaListaAORequest = new StringBuilder();
        prelevaListaAORequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mon=\"http://monitoraggioattivita.axis.chemicalrisk\">");
        prelevaListaAORequest.append("<soapenv:Header/>");
        prelevaListaAORequest.append("<soapenv:Body>");
        prelevaListaAORequest.append("<mon:prelevaListaAttivitaOperatore/>");
        prelevaListaAORequest.append("</soapenv:Body>");
        prelevaListaAORequest.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_MONITORAGGIO_ATTIVITA, prelevaListaAORequest.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    public static int availabilityTestPrelevaAttivitaOperatore(int numberOfIterations) {
        StringBuilder prelevaAttivitaOperatoreRequest = new StringBuilder();
        prelevaAttivitaOperatoreRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mon=\"http://monitoraggioattivita.axis.chemicalrisk\">");
        prelevaAttivitaOperatoreRequest.append("<soapenv:Header/>");
        prelevaAttivitaOperatoreRequest.append("<soapenv:Body>");
        prelevaAttivitaOperatoreRequest.append("<mon:prelevaAttivitaOperatore>");
        prelevaAttivitaOperatoreRequest.append("<mon:id>12345678</mon:id>");
        prelevaAttivitaOperatoreRequest.append("</mon:prelevaAttivitaOperatore>");
        prelevaAttivitaOperatoreRequest.append("</soapenv:Body>");
        prelevaAttivitaOperatoreRequest.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_MONITORAGGIO_ATTIVITA, prelevaAttivitaOperatoreRequest.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }
}
