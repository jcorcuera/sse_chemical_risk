package chemicalrisk.axis.azionicorrettive;

import java.util.Random;
/**
 * Classe che implementa il modulo Azioni Correttive
 * Ogni metodo del modulo ha lo stesso errorRate, definito come variabile globale all'interno della classe.
 * Ogni metodo restituisce una serie di valori in base al valore estratto in modo casuale.
 * 
 */
public class ModuloAzioniCorrettive {

    private int nError = 0;//Conta il numero di errori totali
    private int nCalls = 0;//Conta il numero di volte che si chiama un metodo
    private final double errorRate = 0.001;//99%
    private static final Random RANDOM = new Random();
    private static final String TIPO_MANUALE = "Manuale";
    private static final String TIPO_AUTOMATICA = "Automatica";
    private static final String STATO_PENDENTE = "Pendente";
    private static final String STATO_APPROVATA_NON_EFFETTUATA = "ApprovataNonEffettuata";
    private static final Integer MAX_NUM_RICHIESTE = 5;
    private static final Integer MAX_NUM_AZIONICORRETTIVE = 3;
    private static final Integer MAX_NUM_SCADENZA_RIMANENTI = 3;

    public static final String[] STATI_AZIONE_CORRETTIVA = {STATO_PENDENTE, STATO_APPROVATA_NON_EFFETTUATA};
    public static final String[] TIPO_AZIONE_CORRETTIVA = {TIPO_MANUALE, TIPO_AUTOMATICA};

    public boolean aggiornaStatoAzioneCorrettiva(String aAzioneCorrettiva) {
        nCalls++;
        if (Math.random() <= errorRate) {//webservices non disponibilie
            nError++;
            return false;
        }
        // TESTING_EXAMPLE
        if (aAzioneCorrettiva == null || aAzioneCorrettiva.equals("")) {
            return false;
        }
        if (aAzioneCorrettiva.equals("Effettuata")) {
            return true;
        }

        return true;
    }

    public boolean inserisciRichiesta(String aRichiesta) {
        nCalls++;
        if (Math.random() <= errorRate) {//webservices non disponibilie
            nError++;
            return false;
        }
        // TESTING_EXAMPLE
        if (aRichiesta == null || aRichiesta.equals("")) {
            return false;
        }
        if (aRichiesta.equals("nuovaRichiesta")) {
            return true;
        }
        return true;
    }

    public boolean inserisciAzioneCorrettiva(String aAzioneCorrettiva) {
        nCalls++;
        if (Math.random() <= errorRate) {
            nError++;
            return false;
        }
        // TESTING_EXAMPLE
        if (aAzioneCorrettiva == null || aAzioneCorrettiva.equals("")) {
            return false;
        }
        if (aAzioneCorrettiva.equals("nuovaAzioneCorrettiva")) {
            return true;
        }        
        return true;
    }

    public String mostraListaAzioniCorrettive(String aRichiesta) {
        nCalls++;
        if (Math.random() <= errorRate) {
            nError++;
            return "";
        }
        //TESTING_EXAMPLE
        if (aRichiesta == null || aRichiesta.isEmpty()) {
            return "Errore: parametro ricerca non valido";
        }

        int numAzioniCorrettive = 0 + RANDOM.nextInt(MAX_NUM_AZIONICORRETTIVE);
        // If no "AzioniCorrettive" are present the service returns an empty list
        if (numAzioniCorrettive == 0) {
            return ";";
        }

        String azioniCorrettive = "";
        for (int i = 0; i < numAzioniCorrettive; i++) {
            int indexStato = RANDOM.nextInt(STATI_AZIONE_CORRETTIVA.length);
            int indexTipo = RANDOM.nextInt(TIPO_AZIONE_CORRETTIVA.length);
            int numberOfScadenzaRimanente = RANDOM.nextInt(MAX_NUM_SCADENZA_RIMANENTI);
            String trascorsaDataScandenza = RANDOM.nextBoolean() ? "trascorsaDataScandenza" : "noTrascorsaDataScandenza";
            //tipo 1 == AUTOMATICA
            if (indexTipo == 1){
                trascorsaDataScandenza = "noTrascorsaDataScandenza";
            }
            String azioneCorrettiva = "AC" + (i + 1) + "," + 
                                        trascorsaDataScandenza + "," + 
                                        STATI_AZIONE_CORRETTIVA[indexStato] + "," + 
                                        TIPO_AZIONE_CORRETTIVA[indexTipo] + "," + 
                                        numberOfScadenzaRimanente + ";";
            azioniCorrettive += azioneCorrettiva;
        }
        return azioniCorrettive;
    }

    public String mostraListaRichiesteDaApprovare(String parametro) {
        nCalls++;
        if (Math.random() <= errorRate) {
            nError++;
            return "";
        }
        if (parametro == null || parametro.isEmpty()) {
            return "Errore: parametro ricerca non valido";
        }
        int numRichieste = 0 + RANDOM.nextInt(MAX_NUM_RICHIESTE);
        // If no "RichiesteDaApprovare" are present the service returns an empty list
        if (numRichieste == 0) {
            return ";";
        }

        String richieste = "";
        for (int i = 0; i < numRichieste; i++) {
            /* format R1, 1; */
            /* R1 -> idRichiesta, 1 -> idAttivitaOperatore */
            richieste = richieste + "R" + (i + 1) + "," + (i + 1) + ";";
        }
        return richieste;
    }

    /**
     * Questo metodo crea una stringa con errori e chiamate complessi, e provvede a resettare le statistiche
     * @return Stringa contenente la concatenazione del numero di chiamate e del numero di errori
     */
    public String getStats() {
        int tError = nError;
        int tCalls = nCalls;
        nError = 0;
        nCalls = 0;
        return tCalls + "," + tError;
    }
}