package chemicalrisk.axis.monitoraggioattivita;

public class ModuloMonitoraggioAttivita {

    private int nCalls = 0;
    private int nError = 0;
    private final double errorRate = 0.001;//99.9%

    public boolean bloccaAttivita(String attivitaOperatore) {
        nCalls++;
        if (Math.random() <= errorRate) {    // Web service non disponibile
            nError++;
            return false;
        }

        // If the "attivitaOperatore" parameter is null or empty
        if (attivitaOperatore == null || attivitaOperatore.isEmpty()) {
            return false;
        }

        // TESTING_EXAMPLE
        if (attivitaOperatore.equals("vuotaDataFine,vuotaListaStruProtObblNonPresi,123,0,111-222-333,,15645")) {
            return true;
        }

        return true;
    }

    public String prelevaListaAttivitaOperatore() {
        nCalls++;
        if (Math.random() <= errorRate) { // Web service non disponibile
            nError++;
            return "";
        }
        /* object deliminter ; */

 	/* Response format: dataDiFine, FlagListaStrumentiObbligatoriNonPresi, IDoperatore, OreLavoratePerAttivitaOperatoreAttuale, listaStrumentiDiProtezionePresi, listaStrumentiDiProtezioneRilasciati*/
 	/* flow 1: attivita operatore in corso e listaStruProtObblNonPresi non e' vuota */
        String attivitaOperatore1 = "vuotaDataFine,nonVuotaListaStruProtObblNonPresi,123,0,111-222-333,;";
        /* flow 2: attivita operatore in corso e listaStruProtObblNonPresi e' vuota */
        String attivitaOperatore2 = "vuotaDataFine,vuotaListaStruProtObblNonPresi,123,0,111-222-333,;";
        /* flow 3: attivita operatore al termine e listaStrumentiDiProtezionePresi != listaStrumentiDiProtezioneRilasciati */
        String attivitaOperatore3 = "dataFine,vuotaListaStruProtObblNonPresi,123,8,111-222-333,111;";
        /* flow 4: attivita operatore al termine e listaStrumentiDiProtezionePresi == listaStrumentiDiProtezioneRilasciati */
        String attivitaOperatore4 = "dataFine,vuotaListaStruProtObblNonPresi,123,8,111-222-333,111-222-333;";

        /* flow 1: attivita operatore in corso e listaStruProtObblNonPresi non e' vuota */
        String attivitaOperatore5 = "vuotaDataFine,nonVuotaListaStruProtObblNonPresi,234,0,112-223-334,;";
        /* flow 2: attivita operatore in corso e listaStruProtObblNonPresi e' vuota */
        String attivitaOperatore6 = "vuotaDataFine,vuotaListaStruProtObblNonPresi,234,0,112-223-334,;";
        /* flow 3: attivita operatore al termine e listaStrumentiDiProtezionePresi != listaStrumentiDiProtezioneRilasciati */
        String attivitaOperatore7 = "dataFine,vuotaListaStruProtObblNonPresi,234,8,112-223-334,112-223;";
        /* flow 4: attivita operatore al termine e listaStrumentiDiProtezionePresi == listaStrumentiDiProtezioneRilasciati */
        String attivitaOperatore8 = "dataFine,vuotaListaStruProtObblNonPresi,234,8,112-223-334,112-223-334;";

        /* flow 1: attivita operatore in corso e listaStruProtObblNonPresi non e' vuota */
        String attivitaOperatore9 = "vuotaDataFine,nonVuotaListaStruProtObblNonPresi,500,0,200-230-335,;";
        /* flow 2: attivita operatore in corso e listaStruProtObblNonPresi e' vuota */
        String attivitaOperatore10 = "vuotaDataFine,vuotaListaStruProtObblNonPresi,500,0,200-230-335,;";
        /* flow 3: attivita operatore al termine e listaStrumentiDiProtezionePresi != listaStrumentiDiProtezioneRilasciati */
        String attivitaOperatore11 = "dataFine,vuotaListaStruProtObblNonPresi,500,8,200-230-335,230-335;";
        /* flow 4: attivita operatore al termine e listaStrumentiDiProtezionePresi == listaStrumentiDiProtezioneRilasciati */
        String attivitaOperatore12 = "dataFine,vuotaListaStruProtObblNonPresi,500,8,200-230-335,200-230-335;";

        /* flow 1: attivita operatore in corso e listaStruProtObblNonPresi non e' vuota */
        String attivitaOperatore13 = "vuotaDataFine,nonVuotaListaStruProtObblNonPresi,600,0,205-232-339,;";
        /* flow 2: attivita operatore in corso e listaStruProtObblNonPresi e' vuota */
        String attivitaOperatore14 = "vuotaDataFine,vuotaListaStruProtObblNonPresi,600,0,205-232-339,;";
        /* flow 3: attivita operatore al termine e listaStrumentiDiProtezionePresi != listaStrumentiDiProtezioneRilasciati */
        String attivitaOperatore15 = "dataFine,vuotaListaStruProtObblNonPresi,600,8,205-232-339,232-339;";
        /* flow 4: attivita operatore al termine e listaStrumentiDiProtezionePresi == listaStrumentiDiProtezioneRilasciati */
        String attivitaOperatore16 = "dataFine,vuotaListaStruProtObblNonPresi,600,8,205-232-339,205-232-339;";

        return attivitaOperatore1 + attivitaOperatore2 + attivitaOperatore3 + attivitaOperatore4 + attivitaOperatore5 + attivitaOperatore6 + attivitaOperatore7
                + attivitaOperatore8 + attivitaOperatore9 + attivitaOperatore10 + attivitaOperatore11 + attivitaOperatore12 + attivitaOperatore13
                + attivitaOperatore14 + attivitaOperatore15 + attivitaOperatore16;
    }

    public String prelevaAttivitaOperatore(int id) {
        nCalls++;
        if (Math.random() <= errorRate) { // Web service non disponibile
            nError++;
            return "";
        }
        // If the "id" parameter has a not valid value
        if (id < 0) {
            return "Errore: id non valido";
        }

        // TESTING_EXAMPLE
        if (id == 235) {
            return "dataFine,listaStruProtObblNonPresi,operatoreAO,0,xxx-yyy,;";
        }

        return "dataFine,listaStruProtObblNonPresi,operatoreAO,0,xxx-zzz,;";
    }

    public String getStats() {
        int tError = nError;
        int tCalls = nCalls;
        nError = 0;
        nCalls = 0;
        return tCalls + "," + tError;
    }
}