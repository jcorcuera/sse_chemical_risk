package chemicalrisk.availability;

import chemicalrisk.axis.OrchestratoreAxis;
import java.util.Date;
/**
 * Classe per il calcolo dell'availability dell'intero protocollo
 * Si esegue il protocollo per 4 diverse iterazioni consecutive, elencate nel vettore runs
 * Il main semplimente chiama il metodo Main dell'orchestratore. Ogni qualvolte il protocollo lancia un'eccezione,
 * cioè si verifica un errore, si incrementa una variabile numberOfErrors che conta i fallimenti
 * Al termine si stampa anche il tempo impiegato per eseguire ogni diverso tipo di iterazione
 * 
 **/
public class TestAvailabilityProtocol {

    public static void main(String[] args) throws Exception {
        int[] runs = {4, 100, 1000, 10000};

        System.out.println("# runs \t # errors \t duration (sec) \t % availability");
        for (int r = 0; r < runs.length; r++) {  
            Date dataInizio = new Date();
            int numberOfErrors = 0;
            for (int i = 0; i < runs[r]; i++) {
                try {
                    OrchestratoreAxis.main(null);
                } catch (Exception ex) {
                    //Quando un servizio non è disponibile viene generata un'eccezione e 
                    //l'esecuzione del protocollo corrente viene annullata.
                    numberOfErrors++;
                }
            }
            Date dataFine = new Date();
            long timeSeconds = (long) Math.floor((dataFine.getTime() - dataInizio.getTime()) / 1000.0);    
            double availability = (1 - numberOfErrors*1.0/runs[r]) * 100;
            System.out.println(runs[r] + " \t " + numberOfErrors + " \t " + timeSeconds +" \t " + availability);    
        }
    }
}