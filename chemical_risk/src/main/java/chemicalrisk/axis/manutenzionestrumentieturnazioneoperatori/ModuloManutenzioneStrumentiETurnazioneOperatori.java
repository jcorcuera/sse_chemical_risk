package chemicalrisk.axis.manutenzionestrumentieturnazioneoperatori;

import java.util.Random;

public class ModuloManutenzioneStrumentiETurnazioneOperatori {

    private int nError = 0;
    private int nCalls = 0;
    private final double errorRate = 0.001; // 99.9%
    private static final Random RANDOM = new Random();
    private static final int LIVELLOUSURA_VALORE_MIN = 1;
    private static final int LIVELLOUSURA_VALORE_MAX = 4;
    private static final int LIVELLOSTANCHEZZA_VALORE_MIN = 1;
    private static final int LIVELLOSTANCHEZZA_VALORE_MAX = 4;

    public String prelevaListaManutenzioneStrumenti(String parametri) {
        nCalls++;
        if (Math.random() <= errorRate) {
            nError++;
            return "";
        }
        // Validation to check if parametri has been sent
        if (parametri == null || parametri.isEmpty()) {
            return "Errore: parametri non validi";
        }
        return "ListaManutenzioneStrumenti";
    }

    public String prelevaListaOperatori(String parametri) {
        nCalls++;
        if (Math.random() <= errorRate) {
            nError++;
            return "";
        }
        // Validation to check if parametri has been sent
        if (parametri == null || parametri.isEmpty()) {
            return "Errore: parametri non validi";
        }
        return "ListaOperatori";
    }

    public int aggiornaManutenzioniStrumenti(String nome, int oreDiUtilizzoComplessivo, String ultimaManutenzione) {
        nCalls++;
        if (Math.random() <= errorRate) {
            nError++;
            return -1;
        }
        // TEST SCENARIO: if strumento == 111, service returns LIVELLOUSURA_VALORE_MAX
        if (Math.random() <= 0.03 || nome.equalsIgnoreCase("111")) {
            return LIVELLOUSURA_VALORE_MAX;
        }
        return LIVELLOUSURA_VALORE_MIN + RANDOM.nextInt(LIVELLOUSURA_VALORE_MAX);
    }

    public int aggiornaOperatori(int idOperatore, int oreDiLavoroUltimaSettimana) {
        nCalls++;
        if (Math.random() <= errorRate) {
            nError++;
            return -1;
        }
        // TEST SCENARIO: if operatore == 123, service returns LIVELLOSTANCHEZZA_VALORE_MAX
        if (Math.random() <= 0.1 || idOperatore == 123) {
            return LIVELLOSTANCHEZZA_VALORE_MAX;
        }
        return LIVELLOSTANCHEZZA_VALORE_MIN + RANDOM.nextInt(LIVELLOSTANCHEZZA_VALORE_MAX);
    }

    public String richiediUlterioreManutenzione(String nome, String dataPrevista, int durataPrevista, String priorita) {
        nCalls++;
        if (Math.random() <= errorRate) {
            nError++;
            return "";
        }
        // Validation to check if nome has been sent
        if (nome == null || nome.isEmpty()) {
            return "Errore: nome strumento non valido";
        }
        // Validation to check if dataPrevista has been sent
        if (dataPrevista == null || dataPrevista.isEmpty()) {
            return "Errore: dataPrevista non valida";
        }
        // Validation to check if durataPrevista has been sent
        if (durataPrevista <= 0) {
            return "Errore: durataPrevista non valida";
        }
        // Validation to check if priorita has been sent
        if (priorita == null || priorita.isEmpty()) {
            return "Errore: priorita non valida";
        }
        return "richiediUlterioreManutenzione";
    }

    public String fissaNumeroMassimoOre(int idOperatore, String inizioSettimanaDiRiferimento, int numeroMassimoOreDiLavoro, int numeroGiorniDiRiposo) {
        nCalls++;
        if (Math.random() <= errorRate) {
            nError++;
            return "";
        }
        // Validation to check if idOperatore is not negative
        if (idOperatore <= 0) {
            return "Errore: Id Operatore non valido";
        }
        // Validation to check if inizioSettimanaDiRiferimento has been sent
        if (inizioSettimanaDiRiferimento == null || inizioSettimanaDiRiferimento.isEmpty()) {
            return "Errore: Inizio Settimana Di Riferimento non valido";
        }
        // Validation to check if numeroMassimoOreDiLavoro is not negative
        if (numeroMassimoOreDiLavoro <= 0) {
            return "Errore: Numero Massimo Ore Di Lavoro non valido";
        }
        // Validation to check if numeroGiorniDiRiposo is not negative
        if (numeroGiorniDiRiposo <= 0) {
            return "Errore: Numero Giorni Di Riposo non valido";
        }
        return "fissaNumeroMassimoOre";
    }

    public String getStats() {
        int tError = nError;
        int tCalls = nCalls;
        nError = 0;
        nCalls = 0;
        return tCalls + "," + tError;
    }
}