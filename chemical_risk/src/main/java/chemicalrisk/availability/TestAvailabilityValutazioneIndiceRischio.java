package chemicalrisk.availability;

import chemicalrisk.soap.SOAPClient;

public class TestAvailabilityValutazioneIndiceRischio {

    private static final String WS_VALUTAZIONE_INDICE_RISCHIO = "http://localhost:8080/axis2/services/ModuloValutazioneIndiceRischio";

    public static void main(String[] args) {
        int runs = 10;
        int numberOfIterations = 1000;
        int errors[][] = new int[runs][1];
        double availabilityOperation[][] = new double[runs][1];
        double availabilityWS[] = new double[runs];
        double avgAvailability = 0;
        double avgErrors = 0;

        for (int i = 0; i < runs; i++) {
            errors[i][0] = availabilityCalcoloIndiceRischio(numberOfIterations);

            availabilityOperation[i][0] = (1 - (errors[i][0] * 1.0) / numberOfIterations) * 100;

            availabilityWS[i] = (1 - (errors[i][0]) * 1.0 / numberOfIterations * 1) * 100;
            avgAvailability += availabilityWS[i];
            double errorCount = errors[i][0];
            System.out.println("--- Run #" + (i + 1) + " ---");
            System.out.println("Availability di calcoloIndiceRischio:\t\t" + availabilityOperation[i][0]);
            System.out.println("Numeri d'errori:\t\t" + errorCount);
            System.out.println("Availability totale:\t\t\t" + availabilityWS[i]);
            avgErrors += errorCount;
        }

        avgAvailability = avgAvailability * 1.0 / runs;
        avgErrors = avgErrors * 1.0 / runs;

        System.out.println("Mean:\t" + avgAvailability);
        System.out.println("Avg Errors:\t" + avgErrors);        
    }

    public static int availabilityCalcoloIndiceRischio(int numberOfIterations) {
        StringBuilder message = new StringBuilder();
        message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:val=\"http://valutazioneindicerischio.axis.chemicalrisk\">");
        message.append("<soapenv:Header/>");
        message.append("<soapenv:Body>");
        message.append("<val:calcolaIndiceRischio>");
        message.append("<val:aAttivitaOperatore></val:aAttivitaOperatore>");
        message.append("<val:aListaStrumenti></val:aListaStrumenti>");
        message.append("<val:aOperatore></val:aOperatore>");
        message.append("</val:calcolaIndiceRischio>");
        message.append("</soapenv:Body>");
        message.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_VALUTAZIONE_INDICE_RISCHIO, message.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }
}
