package chemicalrisk.valutazioneindicerischio;

import java.util.Random;

public class ModuloValutazioneIndiceRischio {

    private static final Random RANDOM = new Random();
    private static final int RISCHIO_VALORE_MIN = 0;
    private static final int RISCHIO_VALORE_MAX = 5;
    
    public String calcolaIndiceRischio(String aAttivitaOperatore, String aListaStrumenti, String aOperatore) {
        return (RANDOM.nextInt((RISCHIO_VALORE_MAX - RISCHIO_VALORE_MIN) + 1) + RISCHIO_VALORE_MIN) + "";
    }
    
}
