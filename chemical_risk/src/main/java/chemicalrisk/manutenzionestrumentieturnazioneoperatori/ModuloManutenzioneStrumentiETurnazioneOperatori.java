package chemicalrisk.manutenzionestrumentieturnazioneoperatori;

public class ModuloManutenzioneStrumentiETurnazioneOperatori {
    
    /**
     *
     * @param parametri
     * @return 
     */
    public String prelevaListaManutenzioneStrumenti(String parametri) {
        return "";
    }

    /**
     *
     * @param parametri
     * @return 
     */
    public String prelevaListaOperatori(String parametri) {
        return "";
    }

    /**
     *
     * @param nome
     * @param oreDiUtilizzoComplessivo
     * @param ultimaManutenzione
     * @return 
     */
    public int aggiornaManutenzioniStrumenti(String nome, int oreDiUtilizzoComplessivo, String ultimaManutenzione) {
        return 3;
    }

    /**
     *
     * @param idOperatore
     * @param oreDiLavoroUltimaSettimana
     * @return 
     */
    public int aggiornaOperatori(int idOperatore, int oreDiLavoroUltimaSettimana) {
        return 2;
    }

    /**
     *
     * @param nome
     * @param dataPrevista
     * @param durataPrevista
     * @param priorita
     * @return 
     */
    public String richiediUlterioreManutenzione(String nome, String dataPrevista, int durataPrevista, String priorita) {
        return "";
    }

    /**
     *
     * @param idOperatore
     * @param inizioSettimanaDiRiferimento
     * @param numeroMassimoOreDiLavoro
     * @param numeroGiorniDiRiposo
     * @return 
     */
    public String fissaNumeroMassimoOre(int idOperatore, String inizioSettimanaDiRiferimento, int numeroMassimoOreDiLavoro, int numeroGiorniDiRiposo) {
        return "";
    }
}