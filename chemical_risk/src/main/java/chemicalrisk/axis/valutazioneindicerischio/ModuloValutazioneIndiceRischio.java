package chemicalrisk.axis.valutazioneindicerischio;

import java.util.Random;

public class ModuloValutazioneIndiceRischio {

    private int nCalls = 0;
    private int nError = 0;
    private final double errorRate = 0.001;//99.9%

    private static final Random RANDOM = new Random();
    private static final int RISCHIO_VALORE_MIN = 0;
    private static final int RISCHIO_VALORE_MAX = 5;

    public String calcolaIndiceRischio(String aAttivitaOperatore, String aListaStrumenti, String aOperatore) {
        nCalls++;
        if (Math.random() <= errorRate) {
            nError++;
            return "";
        }
        /* added validation to return an error in case aAttivitaOperatore parameter is not sent */
        if (aAttivitaOperatore == null || aAttivitaOperatore.isEmpty()){
            return "Errore: Attivita Operatore non valido";
        }
        /* added validation to return an error in case aListaStrumenti parameter is not sent */
        if (aListaStrumenti == null || aListaStrumenti.isEmpty()){
            return "Errore: Lista Strumenti non valido";
        }        
        /* added validation to return an error in case aOperatore parameter is not sent */
        if (aOperatore == null || aOperatore.isEmpty()){
            return "Errore: Operatore non valido";
        }   
        /* 
            TESTING EXAMPLE: if this attivitaOperatore is related to operatore 234, it is known that he worked a lot and 
            his level of risk will be the maximum.
        */
        if (aAttivitaOperatore.contains("234")){
            return RISCHIO_VALORE_MAX + "";
        }
        if ((aAttivitaOperatore.contains("vuotaDataFine") && Math.random() <= 0.2)){
            return RISCHIO_VALORE_MAX + "";
        }else if ((aAttivitaOperatore.contains("dataFine") && Math.random() <= 0.15)){
            return RISCHIO_VALORE_MAX + "";
        }
        return (RANDOM.nextInt((RISCHIO_VALORE_MAX - RISCHIO_VALORE_MIN) + 1) + RISCHIO_VALORE_MIN) + "";
    }

    public String getStats() {
        int tError = nError;
        int tCalls = nCalls;
        nError = 0;
        nCalls = 0;
        return tCalls + "," + tError;
    }

}
