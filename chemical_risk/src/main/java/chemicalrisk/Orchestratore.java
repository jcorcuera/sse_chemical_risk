package chemicalrisk;

import chemicalrisk.azionicorrettive.ModuloAzioniCorrettive;
import chemicalrisk.manutenzionestrumentieturnazioneoperatori.ModuloManutenzioneStrumentiETurnazioneOperatori;
import chemicalrisk.monitoraggioattivita.ModuloMonitoraggioAttivita;
import chemicalrisk.valutazioneindicerischio.ModuloValutazioneIndiceRischio;

public class Orchestratore {

    private static final double THRESHOLD_LIVELLO_DI_USURA = 3.5;
    private static final double THRESHOLD_LIVELLO_DI_STANCHEZZA = 3.5;
    private static final int NUM_MASSIMO_ORE_LAVORO = 36;
    private static final int GIORN0_RIPOSO = 1;

    private static final String TIPO_MANUALE = "Manuale";
    private static final String STATO_PENDENTE = "Pendente";
    private static final String STATO_APPROVATA_NON_EFFETTUATA = "ApprovataNonEffettuata";
    private static final String PRIORITA_STRUMENTO_MASSIMA = "Massima";
    private static final Integer DEFAULT_DURATA_MANUTENZIONE = 10;

    private static final int AO_OPERATORE_INDEX = 2;
    private static final int AO_ORELAVORATE_INDEX = 3;
    private static final int AO_STRUMENTIPROTEZIONEPRESI_INDEX = 4;
    private static final int AO_STRUMENTIPROTEZIONERILASCIATI_INDEX = 5;
    private static final String DATA_SCADENZA_TRASCORSA = "trascorsaDataScandenza";
    private static final int AC_NUMERO_SCADENZE_RIMANENTI_INDEX = 4;

    private static final String RISCHIO_LIVELLO_4 = "4";
    private static final String RISCHIO_LIVELLO_5 = "5";

    public static void main(String[] args) {
        ModuloValutazioneIndiceRischio moduloValutazioneIndiceRischio = new ModuloValutazioneIndiceRischio();
        ModuloAzioniCorrettive moduloAzioniCorrettive = new ModuloAzioniCorrettive();
        ModuloMonitoraggioAttivita moduloMonitoraggioAttivita = new ModuloMonitoraggioAttivita();
        ModuloManutenzioneStrumentiETurnazioneOperatori moduleManStrumentiETurnazioneOperatori = new ModuloManutenzioneStrumentiETurnazioneOperatori();

        String[] attivitaOperatoreArray = moduloMonitoraggioAttivita.prelevaListaAttivitaOperatore().split(";");

        /* for each AttivitaOperatore */
        for (String attivitaOperatore : attivitaOperatoreArray) {
            /* get the current attivitaOperatore */
            String[] attivitaOperatoreValues = attivitaOperatore.split(",");
            String operatoreAO = attivitaOperatoreValues[AO_OPERATORE_INDEX];
            String oreLavorate = attivitaOperatoreValues[AO_ORELAVORATE_INDEX];
            /* when attivitaOperatore is in progress */
            if (attivitaOperatore.contains("vuotaDataFine")) {
                if (!attivitaOperatore.contains("vuotaListaStruProtObblNonPresi")) {
                    boolean response = moduloMonitoraggioAttivita.bloccaAttivita(attivitaOperatore);
                } else {
                    /* INIZIO CALCOLO INDICE RISCHIO */
                    String listaStrumentiDiProtezionePresi = attivitaOperatoreValues[AO_STRUMENTIPROTEZIONEPRESI_INDEX];
                    String listaManutenzioneStrumenti = moduleManStrumentiETurnazioneOperatori.prelevaListaManutenzioneStrumenti(listaStrumentiDiProtezionePresi);
                    /* needs to build an object using operatore of attivitaOperatore */
                    String operatoreFromManutenzione = moduleManStrumentiETurnazioneOperatori.prelevaListaOperatori(operatoreAO).split(";")[0];
                    String rischioEntita = moduloValutazioneIndiceRischio.calcolaIndiceRischio(attivitaOperatore, 
                            listaManutenzioneStrumenti, operatoreFromManutenzione);
                    if (rischioEntita.contains(RISCHIO_LIVELLO_4) || rischioEntita.contains(RISCHIO_LIVELLO_5)) {
                        String nuovaRichiesta = "nuovaRichiesta";
                        boolean response = moduloAzioniCorrettive.inserisciRichiesta(nuovaRichiesta);
                        String nuovaAzioneCorrettiva = "nuovaAzioneCorrettiva";
                        response = moduloAzioniCorrettive.inserisciAzioneCorrettiva(nuovaAzioneCorrettiva);
                    }
                    /* FINE CALCOLO INDICE RISCHIO */
                }
            } else {
                /* when attivitaOperatore is it about to finish */
                String listaStrumentiDiProtezionePresi = attivitaOperatoreValues[AO_STRUMENTIPROTEZIONEPRESI_INDEX];
                String listaStrumentiDiProtezioneRilasciati = attivitaOperatoreValues[AO_STRUMENTIPROTEZIONERILASCIATI_INDEX];
                if (!listaStrumentiDiProtezionePresi.equals(listaStrumentiDiProtezioneRilasciati)) {
                    boolean response = moduloMonitoraggioAttivita.bloccaAttivita(attivitaOperatore);
                } else {
                    /* INIZIO CALCOLO INDICE RISCHIO */
                    String parametri = "parametri";
                    String listaManutenzioneStrumenti = moduleManStrumentiETurnazioneOperatori.prelevaListaManutenzioneStrumenti(parametri);
                    parametri = "parametri";
                    String listaOperatori = moduleManStrumentiETurnazioneOperatori.prelevaListaOperatori(parametri);
                    String operatore = "operatoreFromListaOperatori";
                    String rischioEntita = moduloValutazioneIndiceRischio.calcolaIndiceRischio(attivitaOperatore, listaManutenzioneStrumenti, operatore);
                    if (rischioEntita.contains(RISCHIO_LIVELLO_4) || rischioEntita.contains(RISCHIO_LIVELLO_5)) {
                        String nuovaRichiesta = "nuovaRichiesta";
                        boolean response = moduloAzioniCorrettive.inserisciRichiesta(nuovaRichiesta);
                        String nuovaAzioneCorrettiva = "nuovaAzioneCorrettiva";
                        response = moduloAzioniCorrettive.inserisciAzioneCorrettiva(nuovaAzioneCorrettiva);
                    }
                    /* FINE CALCOLO INDICE RISCHIO */
                    String[] strumentiDiProtezionePresiArray = listaStrumentiDiProtezionePresi.split("-");
                    for (String nomeStrumentoPreso : strumentiDiProtezionePresiArray) {
                        String ultimaManutenzione = "ultimaManutenzione";
                        int livelloDiUsura = moduleManStrumentiETurnazioneOperatori.aggiornaManutenzioniStrumenti(nomeStrumentoPreso + "", 
                                new Integer(oreLavorate), ultimaManutenzione);
                        if (livelloDiUsura > THRESHOLD_LIVELLO_DI_USURA) {
                            String dataPrevista = "dataSeguenteSettimana";
                            int durataPrevista = DEFAULT_DURATA_MANUTENZIONE;
                            String priorita = PRIORITA_STRUMENTO_MASSIMA;
                            String response = moduleManStrumentiETurnazioneOperatori.richiediUlterioreManutenzione(nomeStrumentoPreso + "", 
                                    dataPrevista, durataPrevista, priorita);
                        }
                    }
                    int idOperatore = Integer.valueOf(operatoreAO); //dummy id
                    int livelloStanchezza = moduleManStrumentiETurnazioneOperatori.aggiornaOperatori(idOperatore, new Integer(oreLavorate));
                    if (livelloStanchezza > THRESHOLD_LIVELLO_DI_STANCHEZZA) {
                        String inizioSettimanaDiRiferimento = "04/06/2018";
                        int numeroMassimoOreDiLavoro = NUM_MASSIMO_ORE_LAVORO;
                        int numeroGiorniDiRiposo = GIORN0_RIPOSO;
                        String response = moduleManStrumentiETurnazioneOperatori.fissaNumeroMassimoOre(idOperatore, inizioSettimanaDiRiferimento, 
                                numeroMassimoOreDiLavoro, numeroGiorniDiRiposo);
                    }
                }
            }
        }
        String filtroAttivitaOperatoreRichieste = "filtroAttivitaOperatoreRichieste";
        String[] listaRichiesteDaApprovareArr = moduloAzioniCorrettive.mostraListaRichiesteDaApprovare(filtroAttivitaOperatoreRichieste).split(";");
        /* for each AzioneCorretiva */
        for (String richiestaDaApprovare : listaRichiesteDaApprovareArr) {
            int idAttivitaOperatore = Integer.parseInt(richiestaDaApprovare.split(",")[1]);
            String attivitaOperatore = moduloMonitoraggioAttivita.prelevaAttivitaOperatore(idAttivitaOperatore);
            String[] listaAzioniCorrettiveArr = moduloAzioniCorrettive.mostraListaAzioniCorrettive(richiestaDaApprovare).split(";");
            for (String azioneCorrettiva : listaAzioniCorrettiveArr) {
                if (azioneCorrettiva.contains(TIPO_MANUALE) && (azioneCorrettiva.contains(STATO_PENDENTE) || azioneCorrettiva.contains(STATO_APPROVATA_NON_EFFETTUATA))
                        && azioneCorrettiva.contains(DATA_SCADENZA_TRASCORSA)) {
                    int numeroScadenzeRimanenti = Integer.parseInt(azioneCorrettiva.split(",")[AC_NUMERO_SCADENZE_RIMANENTI_INDEX]);
                    if (numeroScadenzeRimanenti == 0) {
                        String nuovaAzioneCorrettiva = "nuovaAzioneCorrettiva";
                        boolean response = moduloAzioniCorrettive.inserisciAzioneCorrettiva(nuovaAzioneCorrettiva);
                    }
                    String response = moduloValutazioneIndiceRischio.calcolaIndiceRischio(attivitaOperatore, null, null);
                }
                boolean answer = moduloAzioniCorrettive.aggiornaStatoAzioneCorrettiva(azioneCorrettiva);
            }
        }
    }
}
