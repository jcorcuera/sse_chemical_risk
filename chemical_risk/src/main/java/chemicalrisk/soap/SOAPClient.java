package chemicalrisk.soap;

import javax.xml.soap.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import javax.xml.transform.dom.*;
import java.io.*;

/**
 * Questa classe esegue ha 2 soli metodi che servono per creare un messaggio SOAP e per serializzarlo 
 *
 **/

public class SOAPClient {

    public static String sendMess(String url, String mess) throws Exception {
        SOAPConnection soapConnection = SOAPConnectionFactory.newInstance().createConnection();
        
        //Ottieni la RISPOSTA DEL SERVER
        SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(mess), url);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapResponse.writeTo(out);
        soapConnection.close();
        String resp = new String(out.toByteArray());//RESPONSE
        
        //ESEGUI IL PARSING PER OTTENERE IL "result" DEL TAG
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(out.toByteArray()));
        NodeList nodeList = document.getElementsByTagName("ns:return");
        if (nodeList.getLength() > 0) {
            Element responseElement = (Element) nodeList.item(0);
            String returnValue = responseElement.getTextContent();
            if (returnValue.isEmpty() || returnValue.equalsIgnoreCase("false") || returnValue.equalsIgnoreCase("-1")) {
                throw new Exception("Error on service");
                //La risposta del web service è negativa nel caso di stringa = -1 e false nel caso di un booleano
            }
            return returnValue;
        }
        return resp;
    }

    private static SOAPMessage createSOAPRequest(String m) throws Exception {
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(m.getBytes()));
        soapMessage.getSOAPPart().setContent(new DOMSource(document));
        soapMessage.saveChanges();
        return soapMessage;
    }

}
