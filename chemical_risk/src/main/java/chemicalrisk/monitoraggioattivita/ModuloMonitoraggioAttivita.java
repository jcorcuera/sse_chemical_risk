package chemicalrisk.monitoraggioattivita;

public class ModuloMonitoraggioAttivita {

    /**
     *
     * @param attivitaOperatore
     * @return 
     */
    public boolean bloccaAttivita(String attivitaOperatore) {
        return true;
    }

    public String prelevaListaAttivitaOperatore() {
        /* object deliminter ; */
        /* flow 1: attivita operatore in corso e listaStruProtObblNonPresi e' vuota */
        String attivitaOperatore1 = "vuotaDataFine,vuotaListaStruProtObblNonPresi,123,0,111-222-333,111-222-333;";
        /* flow 2: attivita operatore per chiudere e listaStruProtObblNonPresi non e' vuota e 
            listaStrumentiDiProtezionePresi non e' uguale a listaStrumentiDiProtezioneRilasciati
        */
        String attivitaOperatore2 = "dataFine,listaStruProtObblNonPresi,124,0,111-333,111;";
        /* flow 3: attivita operatore per chiudere e listaStruProtObblNonPresi e' vuota e 
            listaStrumentiDiProtezionePresi e' uguale a listaStrumentiDiProtezioneRilasciati
        */        
        String attivitaOperatore3 = "dataFine,vuotaListaStruProtObblNonPresi,125,0,111-222-333,111-222-333;";
        return attivitaOperatore1 + attivitaOperatore2 + attivitaOperatore3;
    }
    
    
    public String prelevaAttivitaOperatore(int id){
        /* ritorna data dummy d'un registro attivita operatore*/
        String attivitaOperatore2 = "dataFine,listaStruProtObblNonPresi,operatoreAO,0,xxx-zzz,xxx-yyy;";
	return attivitaOperatore2;
    }    

}
