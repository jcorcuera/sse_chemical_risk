package chemicalrisk.availability;

import chemicalrisk.soap.SOAPClient;

/**
 * Classe per il calcolo dell'availability del modulo Azioni Correttive
 * Si esegue ogni singolo metodo un certo numero di volte uguale per tutti i metodi
 * Ogni metodo contiene la richiesta SOAP già formattata corrispondente e tramite l'utilizzo della classe SOAPClient invia il
 * messaggio al server Tomcat dove è alloggiato il Web Service
 * 
 * Il test funziona nel seguente modo: ogni metodo interroga il web service 1000 volte, e tutto il procedimento è 
 * ripetuto 10 volte
 * 
 **/
public class TestAvailabilityAzioniCorrettive {

    private static final String WS_AZIONI_CORRETTIVE = "http://localhost:8080/axis2/services/ModuloAzioniCorrettive";

    public static void main(String[] args) {

        int runs = 10;
        int numberOfIterations = 1000;
        int errors[][] = new int[runs][5];
        double availabilityOperation[][] = new double[runs][5];
        double availabilityWS[] = new double[runs];
        double avgAvailability = 0;
        double avgErrors = 0;

        for (int i = 0; i < runs; i++) {
            errors[i][0] = availabilityAggiornaStatoAzioneCorrettiva(numberOfIterations);
            errors[i][1] = availabilityInserisciRichiesta(numberOfIterations);
            errors[i][2] = availabilityInserisciAzioneCorrettiva(numberOfIterations);
            errors[i][3] = availabilityMostraListaAzioniCorrettive(numberOfIterations);
            errors[i][4] = availabilityMostraListaRichiesteDaApprovare(numberOfIterations);

            availabilityOperation[i][0] = (1 - (errors[i][0] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][1] = (1 - (errors[i][1] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][2] = (1 - (errors[i][2] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][3] = (1 - (errors[i][3] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][4] = (1 - (errors[i][4] * 1.0) / numberOfIterations) * 100;

            availabilityWS[i] = errors[i][0] + errors[i][1] + errors[i][2] + errors[i][3] + errors[i][4];
            availabilityWS[i] = (1 - availabilityWS[i] * 1.0 / (numberOfIterations * 5)) * 100;

            avgAvailability += availabilityWS[i];
            double errorCount = errors[i][0] + errors[i][1] + errors[i][2] + errors[i][3] + errors[i][4];
            System.out.println("--- Run #" + (i + 1) + " ---");
            System.out.println("Availability di aggiornaStatoAzioneCorrettiva:\t\t" + availabilityOperation[i][0]);
            System.out.println("Availability di inserisciRichiesta:\t" + availabilityOperation[i][1]);
            System.out.println("Availability di inserisciAzioneCorrettiva:\t" + availabilityOperation[i][2]);
            System.out.println("Availability di mostraListaAzioniCorrettive:\t" + availabilityOperation[i][3]);
            System.out.println("Availability di mostraListaRichiesteDaApprovare:\t" + availabilityOperation[i][4]);

            System.out.println("Numeri d'errori:\t\t" + errorCount);
            System.out.println("Availability totale:\t\t\t" + availabilityWS[i]);
            avgErrors += errorCount;
        }

        avgAvailability = avgAvailability * 1.0 / runs;
        avgErrors = avgErrors * 1.0 / runs;

        System.out.println("Mean:\t" + avgAvailability);
        System.out.println("Avg Errors:\t" + avgErrors);

    }

    public static int availabilityAggiornaStatoAzioneCorrettiva(int numberOfIterations) {
        StringBuilder aggiornaStatoAzioneCorrettivaRequest = new StringBuilder();
        aggiornaStatoAzioneCorrettivaRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:azi=\"http://azionicorrettive.axis.chemicalrisk\">");
        aggiornaStatoAzioneCorrettivaRequest.append("<soapenv:Header/>");
        aggiornaStatoAzioneCorrettivaRequest.append("<soapenv:Body>");
        aggiornaStatoAzioneCorrettivaRequest.append("<azi:aggiornaStatoAzioneCorrettiva>");
        aggiornaStatoAzioneCorrettivaRequest.append("<azi:aAzioneCorrettiva>Effettuato</azi:aAzioneCorrettiva>");
        aggiornaStatoAzioneCorrettivaRequest.append("</azi:aggiornaStatoAzioneCorrettiva>");
        aggiornaStatoAzioneCorrettivaRequest.append("</soapenv:Body>");
        aggiornaStatoAzioneCorrettivaRequest.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, aggiornaStatoAzioneCorrettivaRequest.toString());
            } catch (Exception ex) {
                //C'è un'eccezione perchè il SOAPClient lancia un'eccezione ogni volta che c'è un errore nel metodo
                //Esempio: metodo boolean che restituisce false
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    public static int availabilityInserisciRichiesta(int numberOfIterations) {
        StringBuilder inserisciRichiestaRequest = new StringBuilder();
        inserisciRichiestaRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:azi=\"http://azionicorrettive.axis.chemicalrisk\">");
        inserisciRichiestaRequest.append("<soapenv:Header/>");
        inserisciRichiestaRequest.append("<soapenv:Body>");
        inserisciRichiestaRequest.append("<azi:inserisciRichiesta>");
        inserisciRichiestaRequest.append("<azi:aRichiesta>nuovaRichiesta</azi:aRichiesta>");
        inserisciRichiestaRequest.append("</azi:inserisciRichiesta>");
        inserisciRichiestaRequest.append("</soapenv:Body>");
        inserisciRichiestaRequest.append("</soapenv:Envelope>");

        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, inserisciRichiestaRequest.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    public static int availabilityInserisciAzioneCorrettiva(int numberOfIterations) {
        StringBuilder message = new StringBuilder();
        message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:azi=\"http://azionicorrettive.axis.chemicalrisk\">");
        message.append("<soapenv:Header/>");
        message.append("<soapenv:Body>");
        message.append("<azi:inserisciAzioneCorrettiva>");
        message.append("<azi:aAzioneCorrettiva>azioneCorrettive</azi:aAzioneCorrettiva>");
        message.append("</azi:inserisciAzioneCorrettiva>");
        message.append("</soapenv:Body>");
        message.append("</soapenv:Envelope>");

        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, message.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    public static int availabilityMostraListaAzioniCorrettive(int numberOfIterations) {
        StringBuilder mostraListaAzioniCorrettiveRequest = new StringBuilder();
        mostraListaAzioniCorrettiveRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:azi=\"http://azionicorrettive.axis.chemicalrisk\">");
        mostraListaAzioniCorrettiveRequest.append("<soapenv:Header/>");
        mostraListaAzioniCorrettiveRequest.append("<soapenv:Body>");
        mostraListaAzioniCorrettiveRequest.append("<azi:mostraListaAzioniCorrettive>");
        mostraListaAzioniCorrettiveRequest.append("<azi:aRichiesta>richiesta</azi:aRichiesta>");
        mostraListaAzioniCorrettiveRequest.append("</azi:mostraListaAzioniCorrettive>");
        mostraListaAzioniCorrettiveRequest.append("</soapenv:Body>");
        mostraListaAzioniCorrettiveRequest.append("</soapenv:Envelope>");

        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, mostraListaAzioniCorrettiveRequest.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    public static int availabilityMostraListaRichiesteDaApprovare(int numberOfIterations) {
        StringBuilder mostraListaRichiesteDaApprovareRequest = new StringBuilder();
        mostraListaRichiesteDaApprovareRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:azi=\"http://azionicorrettive.axis.chemicalrisk\">");
        mostraListaRichiesteDaApprovareRequest.append("<soapenv:Header/>");
        mostraListaRichiesteDaApprovareRequest.append("<soapenv:Body>");
        mostraListaRichiesteDaApprovareRequest.append("<azi:mostraListaRichiesteDaApprovare>");
        mostraListaRichiesteDaApprovareRequest.append("<azi:parametro>parametro</azi:parametro>");
        mostraListaRichiesteDaApprovareRequest.append("</azi:mostraListaRichiesteDaApprovare>");
        mostraListaRichiesteDaApprovareRequest.append("</soapenv:Body>");
        mostraListaRichiesteDaApprovareRequest.append("</soapenv:Envelope>");

        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, mostraListaRichiesteDaApprovareRequest.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

}
