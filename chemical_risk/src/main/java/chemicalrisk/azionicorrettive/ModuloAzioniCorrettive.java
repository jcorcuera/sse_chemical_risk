package chemicalrisk.azionicorrettive;

import java.util.Random;

/**
 * Classe relativa al deliverable T4, nella quale non ci sono risultati casuali come per il testing
 * Per il resto, i metodo ovviamente sono gli stessi.
 * Le stringhe risultato sono pre-formattate e sempre le stesse per ogni chiamata SOAP
 * 
 */
public class ModuloAzioniCorrettive {

    private static final Random RANDOM = new Random();
    private static final String STATO_PENDENTE = "Pendente";
    private static final String STATO_APPROVATA_NON_EFFETTUATA = "ApprovataNonEffettuata";
    private static final Integer MAX_NUM_RICHIESTE = 100;
    private static final Integer MAX_NUM_AZIONICORRETTIVE = 5;
    private static final Integer MAX_NUM_SCADENZA_RIMANENTI = 3;
    private static final String TIPO_MANUALE = "Manuale";
    private static final String TIPO_AUTOMATICA = "Automatica";

    public static final String[] STATI_AZIONE_CORRETTIVA = {STATO_PENDENTE, STATO_APPROVATA_NON_EFFETTUATA};
    public static final String[] TIPO_AZIONE_CORRETTIVA = {TIPO_MANUALE, TIPO_AUTOMATICA};

    public boolean aggiornaStatoAzioneCorrettiva(String aAzioneCorrettiva) {
        return true;
    }

    public boolean inserisciRichiesta(String aRichiesta) {
        return true;
    }

    public boolean inserisciAzioneCorrettiva(String aAzioneCorrettiva) {
        return true;
    }

    public String mostraListaAzioniCorrettive(String aRichiesta) {
        int numAzioniCorrettive = RANDOM.nextInt(MAX_NUM_AZIONICORRETTIVE);
        String azioniCorrettive = "";
        for (int i = 0; i < numAzioniCorrettive; i++) {
            int indexStato = RANDOM.nextInt(STATI_AZIONE_CORRETTIVA.length);
            int indexTipo = RANDOM.nextInt(TIPO_AZIONE_CORRETTIVA.length);
            int numberOfScadenzaRimanente = RANDOM.nextInt(MAX_NUM_SCADENZA_RIMANENTI);
            String trascorsaDataScandenza = RANDOM.nextBoolean() ? "trascorsaDataScandenza" : "noTrascorsaDataScandenza";
            //tipo 1 == AUTOMATICA
            //Ricorda: un'azione correttiva automatica non può scadere e non ha l'attributo numero scadenze rimanenti
            //Inoltre è approvata sempre! non è mai pendente
            if (indexTipo == 1) {
                trascorsaDataScandenza = "noTrascorsaDataScandenza";
            }
            String azioneCorrettiva = "AC" + (i + 1) + ","
                    + trascorsaDataScandenza + ","
                    + STATI_AZIONE_CORRETTIVA[indexStato] + ","
                    + TIPO_AZIONE_CORRETTIVA[indexTipo] + ","
                    + numberOfScadenzaRimanente + ";";
            azioniCorrettive = azioniCorrettive + azioneCorrettiva;
        }
        return azioniCorrettive;
    }

    public String mostraListaRichiesteDaApprovare(String parametro) {
        int numRichieste = RANDOM.nextInt(MAX_NUM_RICHIESTE);
        String richieste = "";
        for (int i = 0; i < numRichieste; i++) {
            /* format R1, 1; */
 /* R1 -> idRichiesta, 1 -> idAttivitaOperatore */
            richieste = richieste + "R" + (i + 1) + "," + (i + 1) + ";";
        }
        return richieste;
    }
}
