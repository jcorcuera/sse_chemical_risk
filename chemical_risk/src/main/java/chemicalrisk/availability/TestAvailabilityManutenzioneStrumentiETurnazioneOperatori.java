package chemicalrisk.availability;

import chemicalrisk.soap.SOAPClient;

public class TestAvailabilityManutenzioneStrumentiETurnazioneOperatori {

    // URL of the SoaML service
    private static final String WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI = "http://localhost:8080/axis2/services/ModuloManutenzioneStrumentiETurnazioneOperatori";

    public static void main(String[] args) {
        
        int runs = 10;
        int numberOfIterations = 1000;
        int errors[][] = new int[runs][6];
        double availabilityOperation[][] = new double[runs][6];
        double availabilityWS[] = new double[runs];
        double avgAvailability = 0;
        double avgErrors = 0;

        // Perform 10 runs
        for (int i = 0; i < runs; i++) {
            // Test 1000 times each operation of the module and register the number of related errors
            errors[i][0] = availabilityPrelevaListaManutenzioneStrumenti(numberOfIterations);
            errors[i][1] = availabilityPrelevaListaOperatori(numberOfIterations);
            errors[i][2] = availabilityAggiornaManutenzioniStrumenti(numberOfIterations);
            errors[i][3] = availabilityAggiornaOperatori(numberOfIterations);
            errors[i][4] = availabilityRichiediUlterioreManutenzione(numberOfIterations);
            errors[i][5] = availabilityFissaNumeroMassimoOre(numberOfIterations);
            
            // Calculate availability of each operation from #errors
            availabilityOperation[i][0] = (1 - (errors[i][0] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][1] = (1 - (errors[i][1] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][2] = (1 - (errors[i][2] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][3] = (1 - (errors[i][3] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][4] = (1 - (errors[i][4] * 1.0) / numberOfIterations) * 100;
            availabilityOperation[i][5] = (1 - (errors[i][5] * 1.0) / numberOfIterations) * 100;            

            // Calculate the overall availability of the entire module
            availabilityWS[i] = errors[i][0] + errors[i][1] + errors[i][2] + errors[i][3] + errors[i][4] + errors[i][5];
            availabilityWS[i] = (1 - availabilityWS[i] * 1.0 / (numberOfIterations * 6))  * 100;
            avgAvailability += availabilityWS[i];
            double errorCount = errors[i][0] + errors[i][1] + errors[i][2] + errors[i][3] + errors[i][4] + errors[i][5];
            System.out.println("--- Run #" + (i + 1) + " ---");
            System.out.println("Availability di prelevaListaManutenzioneStrumenti:\t\t" + availabilityOperation[i][0]);
            System.out.println("Availability di prelevaListaOperatori:\t" + availabilityOperation[i][1]);
            System.out.println("Availability di aggiornaManutenzioniStrumenti:\t" + availabilityOperation[i][2]);
            System.out.println("Availability di aggiornaOperatori:\t" + availabilityOperation[i][3]);            
            System.out.println("Availability di richiediUlterioreManutenzione:\t" + availabilityOperation[i][4]);            
            System.out.println("Availability di fissaNumeroMassimoOre:\t" + availabilityOperation[i][5]);
            
            System.out.println("Numeri d'errori:\t\t" + errorCount);
            System.out.println("Availability totale:\t\t\t" + availabilityWS[i]);
            avgErrors += errorCount;
        }

        avgAvailability = avgAvailability * 1.0 / runs;
        avgErrors = avgErrors * 1.0 / runs;

        System.out.println("Mean:\t" + avgAvailability);
        System.out.println("Avg Errors:\t" + avgErrors);        

    }

    public static int availabilityPrelevaListaManutenzioneStrumenti(int numberOfIterations) {
        StringBuilder message = new StringBuilder();
        message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
        message.append("<soapenv:Header/>");
        message.append("<soapenv:Body>");
        message.append("<man:prelevaListaManutenzioneStrumenti>");
        message.append("<man:args0>listaStrumenti</man:args0>");
        message.append("</man:prelevaListaManutenzioneStrumenti>");
        message.append("</soapenv:Body>");
        message.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    private static int availabilityPrelevaListaOperatori(int numberOfIterations) {
        StringBuilder message = new StringBuilder();
        message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
        message.append("<soapenv:Header/>");
        message.append("<soapenv:Body>");
        message.append("<man:prelevaListaOperatori>");
        message.append("<man:args0>listaOperatori</man:args0>");
        message.append("</man:prelevaListaOperatori>");
        message.append("</soapenv:Body>");
        message.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    private static int availabilityAggiornaManutenzioniStrumenti(int numberOfIterations) {
        StringBuilder message = new StringBuilder();
        message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
        message.append("<soapenv:Header/>");
        message.append("<soapenv:Body>");
        message.append("<man:aggiornaManutenzioniStrumenti>");
        message.append("<man:args0>nomeStrumentoPreso</man:args0>");
        message.append("<man:args1>12</man:args1>");
        message.append("<man:args2>ultimaManutenzione</man:args2>");
        message.append("</man:aggiornaManutenzioniStrumenti>");
        message.append("</soapenv:Body>");
        message.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    private static int availabilityAggiornaOperatori(int numberOfIterations) {
        StringBuilder message = new StringBuilder();
        message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
        message.append("<soapenv:Header/>");
        message.append("<soapenv:Body>");
        message.append("<man:aggiornaOperatori>");
        message.append("<man:args0>111111</man:args0>");
        message.append("<man:args1>3</man:args1>");
        message.append("</man:aggiornaOperatori>");
        message.append("</soapenv:Body>");
        message.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    private static int availabilityRichiediUlterioreManutenzione(int numberOfIterations) {
        StringBuilder message = new StringBuilder();
        message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
        message.append("<soapenv:Header/>");
        message.append("<soapenv:Body>");
        message.append("<man:richiediUlterioreManutenzione>");
        message.append("<man:args0>nomeStrumentoPreso</man:args0>");
        message.append("<man:args1>dataSeguenteSettimana</man:args1>");
        message.append("<man:args2>16</man:args2>");
        message.append("<man:args3>Massima</man:args3>");
        message.append("</man:richiediUlterioreManutenzione>");
        message.append("</soapenv:Body>");
        message.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

    private static int availabilityFissaNumeroMassimoOre(int numberOfIterations) {
        StringBuilder message = new StringBuilder();
        message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
        message.append("<soapenv:Header/>");
        message.append("<soapenv:Body>");
        message.append("<man:fissaNumeroMassimoOre>");
        message.append("<man:args0>123</man:args0>");
        message.append("<man:args1>inizioSettimanaDiRiferimento</man:args1>");
        message.append("<man:args2>36</man:args2>");
        message.append("<man:args3>1</man:args3>");
        message.append("</man:fissaNumeroMassimoOre>");
        message.append("</soapenv:Body>");
        message.append("</soapenv:Envelope>");
        int numberOfErrors = 0;
        for (int i = 0; i < numberOfIterations; i++) {
            try {
                SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString());
            } catch (Exception ex) {
                numberOfErrors++;
            }
        }
        return numberOfErrors;
    }

}
