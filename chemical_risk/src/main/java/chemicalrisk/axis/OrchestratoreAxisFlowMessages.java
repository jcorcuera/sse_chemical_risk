package chemicalrisk.axis;

import chemicalrisk.soap.SOAPClient;

public class OrchestratoreAxisFlowMessages {

    private static final String WS_MONITORAGGIO_ATTIVITA = "http://localhost:8080/axis2/services/ModuloMonitoraggioAttivita";
    private static final String WS_VALUTAZIONE_INDICE_RISCHIO = "http://localhost:8080/axis2/services/ModuloValutazioneIndiceRischio";
    private static final String WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI = "http://localhost:8080/axis2/services/ModuloManutenzioneStrumentiETurnazioneOperatori";
    private static final String WS_AZIONI_CORRETTIVE = "http://localhost:8080/axis2/services/ModuloAzioniCorrettive";

    private static final double THRESHOLD_LIVELLO_DI_USURA = 3.5;
    private static final double THRESHOLD_LIVELLO_DI_STANCHEZZA = 3.5;
    private static final int NUM_MASSIMO_ORE_LAVORO = 36;
    private static final int GIORN0_RIPOSO = 1;

    private static final String TIPO_MANUALE = "Manuale";
    private static final String STATO_PENDENTE = "Pendente";
    private static final String STATO_APPROVATA_NON_EFFETTUATA = "ApprovataNonEffettuata";
    private static final String PRIORITA_STRUMENTO_MASSIMA = "Massima";
    private static final Integer DEFAULT_DURATA_MANUTENZIONE = 10;

    private static final int AO_OPERATORE_INDEX = 2;
    private static final int AO_ORELAVORATE_INDEX = 3;
    private static final int AO_STRUMENTIPROTEZIONEPRESI_INDEX = 4;
    private static final int AO_STRUMENTIPROTEZIONERILASCIATI_INDEX = 5;
    private static final String DATA_SCADENZA_TRASCORSA = "trascorsaDataScandenza";
    private static final int AC_NUMERO_SCADENZE_RIMANENTI_INDEX = 4;

    private static final String RISCHIO_LIVELLO_4 = "4";
    private static final String RISCHIO_LIVELLO_5 = "5";

    public static void main(String[] args) throws Exception {

        StringBuilder prelevaAttivitaOperatoreRequest = new StringBuilder();
        prelevaAttivitaOperatoreRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mon=\"http://monitoraggioattivita.axis.chemicalrisk\">");
        prelevaAttivitaOperatoreRequest.append("<soapenv:Header/>");
        prelevaAttivitaOperatoreRequest.append("<soapenv:Body>");
        prelevaAttivitaOperatoreRequest.append("<mon:prelevaAttivitaOperatore>");
        prelevaAttivitaOperatoreRequest.append("<mon:id>12345678</mon:id>");
        prelevaAttivitaOperatoreRequest.append("</mon:prelevaAttivitaOperatore>");
        prelevaAttivitaOperatoreRequest.append("</soapenv:Body>");
        prelevaAttivitaOperatoreRequest.append("</soapenv:Envelope>");

        StringBuilder bloccaAttivitaRequest = new StringBuilder();
        bloccaAttivitaRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mon=\"http://monitoraggioattivita.axis.chemicalrisk\">");
        bloccaAttivitaRequest.append("<soapenv:Header/>");
        bloccaAttivitaRequest.append("<soapenv:Body>");
        bloccaAttivitaRequest.append("<mon:bloccaAttivita>");
        bloccaAttivitaRequest.append("<mon:attivitaOperatore>attivitaOperatore</mon:attivitaOperatore>");
        bloccaAttivitaRequest.append("</mon:bloccaAttivita>");
        bloccaAttivitaRequest.append("</soapenv:Body>");
        bloccaAttivitaRequest.append("</soapenv:Envelope>");

        StringBuilder prelevaListaAORequest = new StringBuilder();
        prelevaListaAORequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mon=\"http://monitoraggioattivita.axis.chemicalrisk\">");
        prelevaListaAORequest.append("<soapenv:Header/>");
        prelevaListaAORequest.append("<soapenv:Body>");
        prelevaListaAORequest.append("<mon:prelevaListaAttivitaOperatore/>");
        prelevaListaAORequest.append("</soapenv:Body>");
        prelevaListaAORequest.append("</soapenv:Envelope>");

        StringBuilder mostraListaRichiesteDaApprovareRequest = new StringBuilder();
        mostraListaRichiesteDaApprovareRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:azi=\"http://azionicorrettive.axis.chemicalrisk\">");
        mostraListaRichiesteDaApprovareRequest.append("<soapenv:Header/>");
        mostraListaRichiesteDaApprovareRequest.append("<soapenv:Body>");
        mostraListaRichiesteDaApprovareRequest.append("<azi:mostraListaRichiesteDaApprovare>");
        mostraListaRichiesteDaApprovareRequest.append("<azi:parametro>parametro</azi:parametro>");
        mostraListaRichiesteDaApprovareRequest.append("</azi:mostraListaRichiesteDaApprovare>");
        mostraListaRichiesteDaApprovareRequest.append("</soapenv:Body>");
        mostraListaRichiesteDaApprovareRequest.append("</soapenv:Envelope>");

        StringBuilder mostraListaAzioniCorrettiveRequest = new StringBuilder();
        mostraListaAzioniCorrettiveRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:azi=\"http://azionicorrettive.axis.chemicalrisk\">");
        mostraListaAzioniCorrettiveRequest.append("<soapenv:Header/>");
        mostraListaAzioniCorrettiveRequest.append("<soapenv:Body>");
        mostraListaAzioniCorrettiveRequest.append("<azi:mostraListaAzioniCorrettive>");
        mostraListaAzioniCorrettiveRequest.append("<azi:aRichiesta>richiesta</azi:aRichiesta>");
        mostraListaAzioniCorrettiveRequest.append("</azi:mostraListaAzioniCorrettive>");
        mostraListaAzioniCorrettiveRequest.append("</soapenv:Body>");
        mostraListaAzioniCorrettiveRequest.append("</soapenv:Envelope>");

        StringBuilder aggiornaStatoAzioneCorrettivaRequest = new StringBuilder();
        aggiornaStatoAzioneCorrettivaRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:azi=\"http://azionicorrettive.axis.chemicalrisk\">");
        aggiornaStatoAzioneCorrettivaRequest.append("<soapenv:Header/>");
        aggiornaStatoAzioneCorrettivaRequest.append("<soapenv:Body>");
        aggiornaStatoAzioneCorrettivaRequest.append("<azi:aggiornaStatoAzioneCorrettiva>");
        aggiornaStatoAzioneCorrettivaRequest.append("<azi:aAzioneCorrettiva>Effettuato</azi:aAzioneCorrettiva>");
        aggiornaStatoAzioneCorrettivaRequest.append("</azi:aggiornaStatoAzioneCorrettiva>");
        aggiornaStatoAzioneCorrettivaRequest.append("</soapenv:Body>");
        aggiornaStatoAzioneCorrettivaRequest.append("</soapenv:Envelope>");

        StringBuilder inserisciRichiestaRequest = new StringBuilder();
        inserisciRichiestaRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:azi=\"http://azionicorrettive.axis.chemicalrisk\">");
        inserisciRichiestaRequest.append("<soapenv:Header/>");
        inserisciRichiestaRequest.append("<soapenv:Body>");
        inserisciRichiestaRequest.append("<azi:inserisciRichiesta>");
        inserisciRichiestaRequest.append("<azi:aRichiesta>nuovaRichiesta</azi:aRichiesta>");
        inserisciRichiestaRequest.append("</azi:inserisciRichiesta>");
        inserisciRichiestaRequest.append("</soapenv:Body>");
        inserisciRichiestaRequest.append("</soapenv:Envelope>");

        StringBuilder azioneCorrettivaRequest = new StringBuilder();
        azioneCorrettivaRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:azi=\"http://azionicorrettive.axis.chemicalrisk\">");
        azioneCorrettivaRequest.append("<soapenv:Header/>");
        azioneCorrettivaRequest.append("<soapenv:Body>");
        azioneCorrettivaRequest.append("<azi:inserisciAzioneCorrettiva>");
        azioneCorrettivaRequest.append("<azi:aAzioneCorrettiva>nuovaAzioneCorrettiva</azi:aAzioneCorrettiva>");
        azioneCorrettivaRequest.append("</azi:inserisciAzioneCorrettiva>");
        azioneCorrettivaRequest.append("</soapenv:Body>");
        azioneCorrettivaRequest.append("</soapenv:Envelope>");

        StringBuilder message = null;

        System.out.println("(0) prelevaListaAttivitaOperatore()");
        // prelevaListaAttivitaOperatore
        String[] attivitaOperatoreArray = SOAPClient.sendMess(WS_MONITORAGGIO_ATTIVITA, prelevaListaAORequest.toString()).split(";");
        /* for each AttivitaOperatore */
        System.out.println("(1) " + (attivitaOperatoreArray.length > 0 ? "non" : "") + " vuota listaAttivitaOperatore ");
        for (String attivitaOperatore : attivitaOperatoreArray) {
            System.out.println("======================inizio attivitaOperatore===================================");
            System.out.println("(1.1) attivitaOperatore: " + attivitaOperatore);
            /* get the current attivitaOperatore */
            String[] attivitaOperatoreValues = attivitaOperatore.split(",");
            String operatoreAO = attivitaOperatoreValues[AO_OPERATORE_INDEX];
            String oreLavorate = attivitaOperatoreValues[AO_ORELAVORATE_INDEX];
            /* when attivitaOperatore is in progress */
            if (attivitaOperatore.contains("vuotaDataFine")) {
                System.out.println("(1.2.1) attivitaOperatore: dataFine e' vuota quindi l'attivitaOperatore e' in PROGRESS");
                if (!attivitaOperatore.contains("vuotaListaStruProtObblNonPresi")) {
                    System.out.println("(1.2.1.1) non e' vuota lista strumenti protezione obbligatori non presi");
                    /* bloccaAttivita */
                    System.out.println("(1.2.1.1.1) bloccaAttivita()");
                    boolean response = Boolean.parseBoolean(SOAPClient.sendMess(WS_MONITORAGGIO_ATTIVITA, bloccaAttivitaRequest.toString()));
                } else {
                    System.out.println("(1.2.1.2) e' vuota lista strumenti protezione obbligatori non presi");
                    /* INIZIO CALCOLO INDICE RISCHIO */
                    String listaStrumentiDiProtezionePresi = attivitaOperatoreValues[AO_STRUMENTIPROTEZIONEPRESI_INDEX];
                    /* prelevaListaManutenzioneStrumenti */
                    message = new StringBuilder();
                    message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
                    message.append("<soapenv:Header/>");
                    message.append("<soapenv:Body>");
                    message.append("<man:prelevaListaManutenzioneStrumenti>");
                    message.append("<man:args0>").append(listaStrumentiDiProtezionePresi).append("</man:args0>");
                    message.append("</man:prelevaListaManutenzioneStrumenti>");
                    message.append("</soapenv:Body>");
                    message.append("</soapenv:Envelope>");
                    System.out.println("(1.2.1.2.1) prelevaListaManutenzioneStrumenti()");
                    String listaManutenzioneStrumenti = SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString());
                    /* prelevaListaOperatori */
                    message = new StringBuilder();
                    message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
                    message.append("<soapenv:Header/>");
                    message.append("<soapenv:Body>");
                    message.append("<man:prelevaListaOperatori>");
                    message.append("<man:args0>").append(operatoreAO).append("</man:args0>");
                    message.append("</man:prelevaListaOperatori>");
                    message.append("</soapenv:Body>");
                    message.append("</soapenv:Envelope>");
                    System.out.println("(1.2.1.2.2) prelevaListaOperatori()");
                    String operatoreFromManutenzione = SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString()).split(";")[0];
                    /* calcolaIndiceRischio */
                    message = new StringBuilder();
                    message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:val=\"http://valutazioneindicerischio.axis.chemicalrisk\">");
                    message.append("<soapenv:Header/>");
                    message.append("<soapenv:Body>");
                    message.append("<val:calcolaIndiceRischio>");
                    message.append("<val:aAttivitaOperatore>").append(attivitaOperatore).append("</val:aAttivitaOperatore>");
                    message.append("<val:aListaStrumenti>").append(listaManutenzioneStrumenti).append("</val:aListaStrumenti>");
                    message.append("<val:aOperatore>").append(operatoreFromManutenzione).append("</val:aOperatore>");
                    message.append("</val:calcolaIndiceRischio>");
                    message.append("</soapenv:Body>");
                    message.append("</soapenv:Envelope>");
                    System.out.println("(1.2.1.2.3) calcolaIndiceRischio()");
                    String rischioEntita = SOAPClient.sendMess(WS_VALUTAZIONE_INDICE_RISCHIO, message.toString());
                    if (rischioEntita.contains(RISCHIO_LIVELLO_4) || rischioEntita.contains(RISCHIO_LIVELLO_5)) {
                        System.out.println("(1.2.1.2.4) il livello del indice di rischio ha superato il livello permesso");
                        System.out.println("(1.2.1.2.4.1) in questo caso si crea una richiesta d'azione correttiva. inserisciRichiesta(). ");
                        boolean response = Boolean.parseBoolean(SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, inserisciRichiestaRequest.toString()));
                        System.out.println("(1.2.1.2.4.2) Si crea una azione correttiva a questa richiesta. inserisciAzioneCorrettiva()");
                        response = Boolean.parseBoolean(SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, azioneCorrettivaRequest.toString()));
                    } else {
                        System.out.println("(1.2.1.2.5) il livello del indice di rischio non ha superato il livello permesso");
                    }
                    /* FINE CALCOLO INDICE RISCHIO */
                }
            } else {
                System.out.println("(1.2.2) dataFine non e' vuota quindi l'attivitaOperatore e' gia finita");
                /* when attivitaOperatore is it about to finish */
                String listaStrumentiDiProtezionePresi = attivitaOperatoreValues[AO_STRUMENTIPROTEZIONEPRESI_INDEX];
                String listaStrumentiDiProtezioneRilasciati = attivitaOperatoreValues[AO_STRUMENTIPROTEZIONERILASCIATI_INDEX];
                if (!listaStrumentiDiProtezionePresi.equals(listaStrumentiDiProtezioneRilasciati)) {
                    System.out.println("(1.2.2.1) Operatore non ha rilasciato tutti gli strumenti presi");
                    /* bloccaAttivita */
                    System.out.println("(1.2.2.1.2) bloccaAttivita.");
                    boolean response = Boolean.parseBoolean(SOAPClient.sendMess(WS_MONITORAGGIO_ATTIVITA, bloccaAttivitaRequest.toString()));
                } else {
                    System.out.println("(1.2.2.2) Operatore ha rilasciato tutti gli strumenti presi");
                    /* INIZIO CALCOLO INDICE RISCHIO */
 /* prelevaListaManutenzioneStrumenti */
                    message = new StringBuilder();
                    message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
                    message.append("<soapenv:Header/>");
                    message.append("<soapenv:Body>");
                    message.append("<man:prelevaListaManutenzioneStrumenti>");
                    message.append("<man:args0>").append(listaStrumentiDiProtezionePresi).append("</man:args0>");
                    message.append("</man:prelevaListaManutenzioneStrumenti>");
                    message.append("</soapenv:Body>");
                    message.append("</soapenv:Envelope>");
                    String listaManutenzioneStrumenti = SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString());
                    System.out.println("(1.2.2.2.1) prelevaListaManutenzioneStrumenti()");
                    /* prelevaListaOperatori */
                    message = new StringBuilder();
                    message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
                    message.append("<soapenv:Header/>");
                    message.append("<soapenv:Body>");
                    message.append("<man:prelevaListaOperatori>");
                    message.append("<man:args0>").append(operatoreAO).append("</man:args0>");
                    message.append("</man:prelevaListaOperatori>");
                    message.append("</soapenv:Body>");
                    message.append("</soapenv:Envelope>");
                    String operatoreFromManutenzione = SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString()).split(";")[0];
                    System.out.println("(1.2.2.2.2) prelevaListaOperatori()");
                    /* calcolaIndiceRischio */
                    message = new StringBuilder();
                    message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:val=\"http://valutazioneindicerischio.axis.chemicalrisk\">");
                    message.append("<soapenv:Header/>");
                    message.append("<soapenv:Body>");
                    message.append("<val:calcolaIndiceRischio>");
                    message.append("<val:aAttivitaOperatore>").append(attivitaOperatore).append("</val:aAttivitaOperatore>");
                    message.append("<val:aListaStrumenti>").append(listaManutenzioneStrumenti).append("</val:aListaStrumenti>");
                    message.append("<val:aOperatore>").append(operatoreFromManutenzione).append("</val:aOperatore>");
                    message.append("</val:calcolaIndiceRischio>");
                    message.append("</soapenv:Body>");
                    message.append("</soapenv:Envelope>");
                    String rischioEntita = SOAPClient.sendMess(WS_VALUTAZIONE_INDICE_RISCHIO, message.toString());
                    System.out.println("(1.2.2.2.3) calcolaIndiceRischio()");
                    if (rischioEntita.contains(RISCHIO_LIVELLO_4) || rischioEntita.contains(RISCHIO_LIVELLO_5)) {
                        System.out.println("(1.2.2.2.4) il livello del indice di rischio ha superato il livello permesso");
                        System.out.println("(1.2.2.2.4.1) in questo caso si crea una richiesta d'azione correttiva. inserisciRichiesta(). ");
                        boolean response = Boolean.parseBoolean(SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, inserisciRichiestaRequest.toString()));
                        System.out.println("(1.2.2.2.4.2) Si crea una azione correttiva a questa richiesta. inserisciAzioneCorrettiva()");
                        response = Boolean.parseBoolean(SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, azioneCorrettivaRequest.toString()));
                    } else {
                        System.out.println("(1.2.2.2.5) il livello del indice di rischio non ha superato il livello permesso");
                    }
                    /* FINE CALCOLO INDICE RISCHIO */
                    String[] strumentiDiProtezionePresiArray = listaStrumentiDiProtezionePresi.split("-");
                    System.out.println("(1.2.2.2.6) " + (strumentiDiProtezionePresiArray.length > 0 ? "non" : "") + " vuota listaStrumentiDiProtezionePresi ");
                    for (String nomeStrumentoPreso : strumentiDiProtezionePresiArray) {
                        System.out.println("(1.2.2.2.6.1) strumentoPreso: " + nomeStrumentoPreso);
                        String ultimaManutenzione = "ultimaManutenzione";
                        /* aggiornaManutenzioniStrumenti */
                        message = new StringBuilder();
                        message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
                        message.append("<soapenv:Header/>");
                        message.append("<soapenv:Body>");
                        message.append("<man:aggiornaManutenzioniStrumenti>");
                        message.append("<man:args0>").append(nomeStrumentoPreso).append("</man:args0>");
                        message.append("<man:args1>").append(oreLavorate).append("</man:args1>");
                        message.append("<man:args2>").append(ultimaManutenzione).append("</man:args2>");
                        message.append("</man:aggiornaManutenzioniStrumenti>");
                        message.append("</soapenv:Body>");
                        message.append("</soapenv:Envelope>");
                        System.out.println("(1.2.2.2.6.2) aggiornaManutenzioniStrumenti()");
                        int livelloDiUsura = Integer.parseInt(SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString()));
                        if (livelloDiUsura > THRESHOLD_LIVELLO_DI_USURA) {
                            System.out.println("(1.2.2.2.6.3) livello di usura strumento supera il livello permesso");
                            /* richiediUlterioreManutenzione */
                            message = new StringBuilder();
                            message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
                            message.append("<soapenv:Header/>");
                            message.append("<soapenv:Body>");
                            message.append("<man:richiediUlterioreManutenzione>");
                            message.append("<man:args0>").append(nomeStrumentoPreso).append("</man:args0>");
                            message.append("<man:args1>").append("dataSeguenteSettimana").append("</man:args1>");
                            message.append("<man:args2>").append(DEFAULT_DURATA_MANUTENZIONE).append("</man:args2>");
                            message.append("<man:args3>").append(PRIORITA_STRUMENTO_MASSIMA).append("</man:args3>");
                            message.append("</man:richiediUlterioreManutenzione>");
                            message.append("</soapenv:Body>");
                            message.append("</soapenv:Envelope>");
                            System.out.println("(1.2.2.2.6.3.1) richiediUlterioreManutenzione()");
                            String response = SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString());
                        } else {
                            System.out.println("(1.2.2.2.6.4) livello di usura strumento non supera il livello permesso");
                        }
                    }
                    System.out.println("(1.2.2.2.7) aggiornaOperatori()");
                    /* aggiornaOperatori */
                    int idOperatore = Integer.valueOf(operatoreAO);
                    message = new StringBuilder();
                    message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
                    message.append("<soapenv:Header/>");
                    message.append("<soapenv:Body>");
                    message.append("<man:aggiornaOperatori>");
                    message.append("<man:args0>").append(idOperatore).append("</man:args0>");
                    message.append("<man:args1>").append(oreLavorate).append("</man:args1>");
                    message.append("</man:aggiornaOperatori>");
                    message.append("</soapenv:Body>");
                    message.append("</soapenv:Envelope>");
                    int livelloStanchezza = Integer.parseInt(SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString()));
                    if (livelloStanchezza > THRESHOLD_LIVELLO_DI_STANCHEZZA) {
                        System.out.println("(1.2.2.2.8) livello di stanchezza supera il livello permesso");
                        String inizioSettimanaDiRiferimento = "04/06/2018";
                        System.out.println("(1.2.2.2.8.1) fissaNumeroMassimoOre()");
                        /* fissaNumeroMassimoOre */
                        message = new StringBuilder();
                        message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:man=\"http://manutenzionestrumentieturnazioneoperatori.axis.chemicalrisk\">");
                        message.append("<soapenv:Header/>");
                        message.append("<soapenv:Body>");
                        message.append("<man:fissaNumeroMassimoOre>");
                        message.append("<man:args0>").append(idOperatore).append("</man:args0>");
                        message.append("<man:args1>").append(inizioSettimanaDiRiferimento).append("</man:args1>");
                        message.append("<man:args2>").append(NUM_MASSIMO_ORE_LAVORO).append("</man:args2>");
                        message.append("<man:args3>").append(GIORN0_RIPOSO).append("</man:args3>");
                        message.append("</man:fissaNumeroMassimoOre>");
                        message.append("</soapenv:Body>");
                        message.append("</soapenv:Envelope>");
                        String response = SOAPClient.sendMess(WS_MANUTENZIONE_STRUMENTI_E_TURNAZIONE_OPERATORI, message.toString());
                    } else {
                        System.out.println("(1.2.2.2.8) livello di stanchezza non supera il livello permesso");
                    }
                }
            }
            System.out.println("=========================fine attivitaOperatore================================");
        }

        System.out.println("(2) mostraListaRichiesteDaApprovare()");
        String[] listaRichiesteDaApprovareArr = SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, mostraListaRichiesteDaApprovareRequest.toString()).split(";");
        //System.out.println("Total number of richistaDaApprovare: " + listaRichiesteDaApprovareArr.length);
        /* for each richistaDaApprovare */
        System.out.println("(3) " + (listaRichiesteDaApprovareArr.length > 0 ? "non" : "") + " vuota listaRichiesteDaApprovare ");
        for (String richiestaDaApprovare : listaRichiesteDaApprovareArr) {
            System.out.println("=========================inizio richiestaDaApprovare================================");
            System.out.println("(2.1) richiestaDaApprovare: " + richiestaDaApprovare);
            int idAttivitaOperatore = Integer.parseInt(richiestaDaApprovare.split(",")[1]);
            /* prelevaAttivitaOperatore */
            System.out.println("(2.2) prelevaAttivitaOperatore()");
            String attivitaOperatore = SOAPClient.sendMess(WS_MONITORAGGIO_ATTIVITA, prelevaAttivitaOperatoreRequest.toString());
            System.out.println("(2.3) mostraListaAzioniCorrettive()");
            String[] listaAzioniCorrettiveArr = SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, mostraListaAzioniCorrettiveRequest.toString()).split(";");
            System.out.println("(2.4) " + (listaAzioniCorrettiveArr.length > 0 ? "non " : "") + "vuota listaAzioniCorrettiveArr ");
            for (String azioneCorrettiva : listaAzioniCorrettiveArr) {
                System.out.println("=========================inizio azioneCorrettiva================================");
                System.out.println("(2.4.1) azioneCorrettiva: " + azioneCorrettiva);
                if (azioneCorrettiva.contains(TIPO_MANUALE) && (azioneCorrettiva.contains(STATO_PENDENTE) || azioneCorrettiva.contains(STATO_APPROVATA_NON_EFFETTUATA)) && azioneCorrettiva.contains(DATA_SCADENZA_TRASCORSA)) {
                    System.out.println("(2.4.2.1) tipo = MANUALE and stato in (PENDENTE, APPROVATA_NON_EFFETTUATA) and trascorsaDataScandenza");
                    int numeroScadenzeRimanenti = Integer.parseInt(azioneCorrettiva.split(",")[AC_NUMERO_SCADENZE_RIMANENTI_INDEX]);
                    if (numeroScadenzeRimanenti == 0) {
                        System.out.println("(2.4.2.1.1) numeroScadenzeRimanenti = 0 ");
                        System.out.println("(2.4.2.1.2) inserisciAzioneCorrettiva() automatica ");
                        boolean response = Boolean.parseBoolean(SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, azioneCorrettivaRequest.toString()));
                    }else{
                        System.out.println("(2.4.2.2.1) numeroScadenzeRimanenti != 0 ");
                    }
                    /* calcolaIndiceRischio */
                    message = new StringBuilder();
                    message.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:val=\"http://valutazioneindicerischio.axis.chemicalrisk\">");
                    message.append("<soapenv:Header/>");
                    message.append("<soapenv:Body>");
                    message.append("<val:calcolaIndiceRischio>");
                    message.append("<val:aAttivitaOperatore>").append(attivitaOperatore).append("</val:aAttivitaOperatore>");
                    message.append("<val:aListaStrumenti></val:aListaStrumenti>");
                    message.append("<val:aOperatore></val:aOperatore>");
                    message.append("</val:calcolaIndiceRischio>");
                    message.append("</soapenv:Body>");
                    message.append("</soapenv:Envelope>");
                    System.out.println("(2.4.2.2) calcolaIndiceRischio()");
                    String rischioEntita = SOAPClient.sendMess(WS_VALUTAZIONE_INDICE_RISCHIO, message.toString());
                }else{
                    System.out.println("(2.4.2.1) NON (tipo = MANUALE and stato in (PENDENTE, APPROVATA_NON_EFFETTUATA) and trascorsaDataScandenza)");
                }
                System.out.println("(2.4.3) aggiornaStatoAzioneCorrettiva()");
                boolean answer = Boolean.parseBoolean(SOAPClient.sendMess(WS_AZIONI_CORRETTIVE, aggiornaStatoAzioneCorrettivaRequest.toString()));
                System.out.println("=========================fine azioneCorrettiva================================");
            }
            System.out.println("=========================fine richiestaDaApprovare================================");
        }
    }
}
